package com.example.apinotes;

import com.example.apinotes.model.Note;
import com.example.apinotes.repository.NoteRepository;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;

@SpringBootApplication
@AllArgsConstructor
public class ApiNotesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiNotesApplication.class, args);
    }

    private NoteRepository noteRepository;
    @Bean
    public CommandLineRunner loadData() {
        return (args) -> {
            noteRepository.save(new Note(null, "text1", LocalDateTime.now().withNano(0), LocalDateTime.now().withNano(0)));
            noteRepository.save(new Note(null, "text2", LocalDateTime.now().withNano(0), LocalDateTime.now().withNano(0)));
            noteRepository.save(new Note(null, "text3", LocalDateTime.now().withNano(0), LocalDateTime.now().withNano(0)));
        };
    }
}
