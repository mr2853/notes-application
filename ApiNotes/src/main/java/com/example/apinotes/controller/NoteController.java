package com.example.apinotes.controller;

import com.example.apinotes.model.Note;
import com.example.apinotes.service.NoteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Optional;

@RestController
@RequestMapping(value = "")
public class NoteController {
    private final NoteService service;
    private ObjectMapper objectMapper;
    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public NoteController(NoteService service) {
        this.service = service;
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        this.objectMapper = JsonMapper.builder()
                .addModule(new JavaTimeModule())
                .build();
        objectMapper.setDateFormat(df);
    }

    @GetMapping(path = "/")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Iterable<Note>> findAll()
    {
        Iterable<Note> all = service.findAll();
        return new ResponseEntity<>(all, HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Optional<Note> getOne(@PathVariable("id") Long id){
        return service.findById(id);
    }

    @PostMapping(path = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public Note create(@RequestBody Note element){
        return service.add(element);
    }

    @PutMapping(path = "/")
    @ResponseStatus(HttpStatus.OK)
    public Note update(@RequestBody Note changedT){
        return service.update(changedT);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") Long id){
        service.deleteById(id);
    }
}
