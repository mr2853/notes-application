package com.example.notesapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity {
    private ArrayList<Note> notes;
    private LinearLayout notesWrapper;
    private String font;
    private static final String URL = "http://10.0.2.2:8080/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        notesWrapper = findViewById(R.id.mainLayout);
        extractBundle();
        getData();
    }

    protected void extractBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            font = bundle.containsKey("font")
                    ? bundle.getString("font")
                    : "";
        }else{
            font = "";
        }
    }

    protected void getData() {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(URL)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                if(response.isSuccessful()){
                    String myResponse = response.body().string();
                    notes = Note.convertJsonToList(myResponse);
                    generateViews();
                    setupCreate();
                    setupSettings();
                }
            }
        });
    }

    protected void generateViews() {
        LayoutInflater inflater = getLayoutInflater();

        for (Note note: notes) {
            View view = inflater.inflate(R.layout.note_layout, notesWrapper, false);
            fillNoteView(note, view);
            setupUpdate(note, view);
            setupDelete(note, view);
            runOnUiThread(() -> notesWrapper.addView(view));
        }
    }

    public static Typeface getTypeface(String font1, Context context){
        Typeface typeface = ResourcesCompat.getFont(context, R.font.abril_fatface);
        switch (font1) {
            case "advent_pro_thin":
                typeface = ResourcesCompat.getFont(context, R.font.advent_pro_thin);
                break;
            case "bad_script":
                typeface = ResourcesCompat.getFont(context, R.font.bad_script);
                break;
            case "bowlby_one":
                typeface = ResourcesCompat.getFont(context, R.font.bowlby_one);
                break;
            case "cherry_cream_soda":
                typeface = ResourcesCompat.getFont(context, R.font.cherry_cream_soda);
                break;
        }
        return typeface;
    }

    protected void fillNoteView(Note c, View v) {
        TextView text = v.findViewById(R.id.text_box);
        TextView dateCreation = v.findViewById(R.id.dateCreation);
        TextView dateModify = v.findViewById(R.id.dateModify);

        text.setText(c.getText());
        text.setTypeface(getTypeface(font, getApplicationContext()));

        dateCreation.setText(String.format("Creation date:\n%s", c.getDateCreation().toString()));
        dateModify.setText(String.format("Modify date:\n%s", c.getDateModify().toString()));
    }

    protected void setupCreate() {
        Button create = findViewById(R.id.btn_add);

        create.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable("notes", notes);
            bundle.putString("font", font);

            Intent intent = new Intent(getApplicationContext(), EditNoteActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        });
    }

    protected void setupSettings() {
        Button settings = findViewById(R.id.btn_settings);

        settings.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable("notes", notes);
            bundle.putString("font", font);

            Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        });
    }

    protected void setupUpdate(Note c, View v) {
        v.findViewById(R.id.btn_edit).setOnClickListener(v1 -> {
            Bundle bundle = new Bundle();
            bundle.putInt("index", notes.indexOf(c));
            bundle.putSerializable("notes", notes);
            bundle.putString("font", font);

            Intent intent = new Intent(getApplicationContext(), EditNoteActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        });
    }

    protected void setupDelete(Note note, View v) {
        Button remove = v.findViewById(R.id.btn_delete);

        remove.setOnClickListener(v1 -> {
            deleteData(note);
        });
    }

    protected void deleteData(Note note){
        notesWrapper.removeViewAt(notes.indexOf(note));
        notes.remove(note);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(String.format("%s%s", URL, note.getId()))
                .delete()
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
            }
        });
    }

}