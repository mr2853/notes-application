package com.example.apinotes.service;

import com.example.apinotes.model.Note;
import com.example.apinotes.repository.NoteRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class NoteService {
    private NoteRepository noteRepository;

    public NoteService(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    public Iterable<Note> findAll(){
        return noteRepository.findAll();
    }

    public Optional<Note> findById(Long id){
        return noteRepository.findById(id);
    }

    public void deleteById(Long id){
        noteRepository.deleteById(id);
    }

    public Note add(Note note){
        note.setDateCreation(LocalDateTime.now().withNano(0));
        note.setDateModify(LocalDateTime.now().withNano(0));
        return noteRepository.save(note);
    }

    public Note update(Note note){
        note.setDateModify(LocalDateTime.now().withNano(0));
        return noteRepository.save(note);
    }
}
