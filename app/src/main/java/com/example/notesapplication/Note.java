package com.example.notesapplication;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.json.JSONException;
import org.json.JSONObject;

public class Note implements Serializable {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("text")
    private String text;
    @JsonProperty("dateCreation")
    private LocalDateTime dateCreation;
    @JsonProperty("dateModify")
    private LocalDateTime dateModify;


    public Note() {
    }

    public Note(Note note) {
        id = note.getId();
        text = note.getText();
        dateCreation = note.getDateCreation();
        dateModify = note.getDateModify();
    }

    public Note(Long id, String text, LocalDateTime dateCreation, LocalDateTime dateModify) {
        this.id = id;
        this.text = text;
        this.dateCreation = dateCreation;
        this.dateModify = dateModify;
    }

    public static ArrayList<Note> convertJsonToList(String json) {
        ArrayList<String> stringList = new ArrayList<>();
        while (json.length() > 10){
            json = json.substring(json.indexOf("{"));
            int index = json.indexOf("}") + 1;
            String part = json.substring(0, index);
            stringList.add(part);
            json = json.substring(index);
        }

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        ArrayList<Note> notes = new ArrayList<>();
        try {
            for(String string : stringList){
                notes.add(mapper.readValue(string, Note.class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return notes;
    }
    public String toJson() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
            String creationTime = null;
            String modifyTime = null;
            if (dateCreation != null){
                creationTime = "\"" + dateCreation.format(formatter) + "\"";
                modifyTime = "\"" + dateModify.format(formatter) + "\"";
            }
            return "{" +
                    "\"id\":" + id +
                    ", \"text\":\"" + text + "\"" +
                    ", \"dateCreation\":" + creationTime  +
                    ", \"dateModify\":" + modifyTime  +
                    "}";
        }
        return null;
    }


    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", dateCreation=" + dateCreation +
                ", dateModify=" + dateModify +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Note note = (Note) o;
        return Objects.equals(id, note.id) && Objects.equals(text, note.text) && Objects.equals(dateCreation, note.dateCreation) && Objects.equals(dateModify, note.dateModify);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, dateCreation, dateModify);
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("text")
    public String getText() {
        return text;
    }

    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("dateCreation")
    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    @JsonProperty("dateCreation")
    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonProperty("dateModify")
    public LocalDateTime getDateModify() {
        return dateModify;
    }

    @JsonProperty("dateModify")
    public void setDateModify(LocalDateTime dateModify) {
        this.dateModify = dateModify;
    }

}
