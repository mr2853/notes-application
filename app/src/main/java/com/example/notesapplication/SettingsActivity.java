package com.example.notesapplication;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.ArrayList;
import java.util.Arrays;

public class SettingsActivity extends AppCompatActivity {
    private ArrayList<Note> notes;
    private LinearLayout notesWrapper;
    private RadioGroup radioGroup;
    private String font;
    private ArrayList<String> fonts = new ArrayList<>(Arrays.asList("advent_pro_thin", "bad_script", "bowlby_one", "cherry_cream_soda", "abril_fatface"));
    private ArrayList<RadioButton> radioButtons = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        extractBundle();
        initialize();
    }


    protected void extractBundle() {
        Bundle bundle = getIntent().getExtras();
        notes = (ArrayList<Note>) bundle.get("notes");
        font = bundle.getString("font");
    }

    protected void initialize() {
        notesWrapper = findViewById(R.id.settingsLayout);
        radioGroup = findViewById(R.id.radioGroup);

        for(String font1 : fonts){
            RadioButton radioButton = new RadioButton(getApplicationContext());
            radioButton.setText(font1);
            radioGroup.addView(radioButton);

            if(font1.equals(font)){
                radioButton.setChecked(true);
            }
            radioButtons.add(radioButton);
        }

        Button accept = new Button(getApplicationContext());
        Button cancel = new Button(getApplicationContext());
        accept.setText("Accept");
        cancel.setText("Cancel");
        listenersBtn(accept, cancel);
        notesWrapper.addView(accept);
        notesWrapper.addView(cancel);
    }

    protected void listenersBtn(Button accept, Button cancel) {
        accept.setOnClickListener(v -> {
            fetchInput();

            Bundle bundle = new Bundle();
            bundle.putSerializable("notes", notes);
            RadioButton radioButton = findViewById(radioGroup.getCheckedRadioButtonId());
            bundle.putString("font", (String) radioButton.getText());

            Intent intent = new Intent(getApplicationContext(), MainActivity.class); // TODO: create method startActivity
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        });

        cancel.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable("notes", notes);
            bundle.putString("font", font);

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        });
    }


    protected String fetchInput() {
        for(RadioButton radioButton : radioButtons){
            if(radioButton.isChecked()){
                return (String) radioButton.getText();
            }
        }
        return "";
    }

}
