package com.example.notesapplication;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;


public class EditNoteActivity extends AppCompatActivity {
    private EditText inputText;
    private ArrayList<Note> notes;
    private Note selectedNote;
    private int noteIndex;
    private String font;
    private static final String URL = "http://10.0.2.2:8080/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        extractBundle();
        initialize();
        if (savedInstanceState == null) {
            fillInput();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (selectedNote != null) {
            fetchInput();
            outState.putParcelable("note", (Parcelable) selectedNote);
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null && savedInstanceState.containsKey("note")) {
            selectedNote = savedInstanceState.getParcelable("note");
            fillInput();
        }
    }

    protected void extractBundle() {
        Bundle bundle = getIntent().getExtras();

        notes = (ArrayList<Note>) bundle.get("notes");
        font = bundle.getString("font");
        noteIndex = bundle.containsKey("index")
                ? bundle.getInt("index")
                : -1;

        if (noteIndex != -1) {
            selectedNote = new Note(notes.get(noteIndex));
        }
    }

    protected void initialize() {
        inputText = findViewById(R.id.inputText);
        inputText.setTypeface(MainActivity.getTypeface(font, getApplicationContext()));
        setupAccept();
        setupCancel();
    }

    protected void fillInput() {
        if (selectedNote == null) {
            return;
        }

        inputText.setText(selectedNote.getText());
    }

    protected void fetchInput() {
        if (selectedNote == null) {
            selectedNote = new Note();
        }

        selectedNote.setText(inputText.getText().toString());
    }

    protected void saveData() throws JsonProcessingException {
        OkHttpClient client = new OkHttpClient();

        String json = selectedNote.toJson();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        RequestBody body = RequestBody.Companion.create(json, JSON);
        Request request = null;

        if (noteIndex == -1) {
            request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .build();
        }
        else {
            request = new Request.Builder()
                    .url(URL)
                    .put(body)
                    .build();
        }



        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                e.getStackTrace();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {
                if (response.isSuccessful()) {
                    ResponseBody responseBody = response.body();
                    String s = responseBody.toString();
                }
            }
        });
    }

    protected void setupAccept() {
        Button accept = findViewById(R.id.btn_save);
        accept.setOnClickListener(v -> {
            fetchInput();

            if (selectedNote.getText().length() == 0) {
                Toast.makeText(getApplicationContext(),"Text polje ne sme da bude prazno", Toast.LENGTH_SHORT).show();
                return;
            }
            try {
                saveData();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            if (noteIndex == -1) {
                notes.add(selectedNote);
            }
            else {
                notes.set(noteIndex, selectedNote);
            }

            Bundle bundle = new Bundle();
            bundle.putSerializable("notes", notes);
            bundle.putString("font", font);

            Intent intent = new Intent(EditNoteActivity.this, MainActivity.class); // TODO: create method startActivity
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        });
    }

    protected void setupCancel() {
        Button cancel = findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable("notes", notes);
            bundle.putString("font", font);

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        });
    }
}
